This chat has only been tested on Chrome and Firefox. Also make sure to be online when you run this, since it uses a few js files from external sites.

This chat uses the WebSocket protocol through Socket.IO (http://socket.io), which is a JavaScript library (that can be run on the server) for realtime web applications.
Socket.IO is a package of nodejs (http://nodejs.org), and can be installed with the npm (http://npmjs.org) tool. 

CryptoJS 3 (http://code.google.com/p/crypto-js) library is used for hashing and encrypting. Only SHA-512 and AES-256 is used in this application. 

Once the server is up by running "node server.js", the chat can be accessed by going to http://127.0.0.1:1337
To begin to chat, users must share a password with each other before-hand, preferably in person. They will also need a chat room to agree on, although this does not need to be confidential. The username can be anything they choose.

Once those 3 fields have been entered, the password is hashed using SHA512. Using that hashed password, the username is then encrypted using AES. At this points we have all we need to start the chat. The room and encrypted username is sent to the server. This will provice privacy for the user since only the encrypted username will be sent through the network and logged on the server.
Other users, that wish to join the chat, will need to do the same thing.

Once a message from the user is submitted, it is encrypted through CryptoJS.AES.encrypt("Message", hashed-pass). The same is done with the username.
The other people in the room will use their hashed pass to decrypt both the message and the username, CryptoJS.AES.decrypt(encrypted-message , hashed-pass).

When the chat is closed, Socket.IO discards the socket. If you come back to the chat you will need to renter your credentials.