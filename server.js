/* Eldo Mata
   Novermber 29, 2012
 */

var http = require('http');
var io = require('socket.io');
var fs = require('fs');

// Send and display chat html file to user
function sendHtml(response) {
	if (sendHtml.cachedHtml) {
		response.writeHead(200, {'Content-Type': 'text/html', 'Transfer-Encoding': 'chunked'});
		response.write(sendHtml.cachedHtml);
		response.end();
	} else {
		fs.readFile('chat.html', function(err, data) {
			sendHtml.cachedHtml = data;
			sendHtml(response);
		});
	}
}

// Create server
var server = http.createServer(function(request, response) {
	//domain check, if we had a real one
	if (request.url == '/') {
		sendHtml(response);
	}
});

// Listening for socket.io events
var soc = io.listen(server);

soc.of('/chat').on('connection', function(socket) {
	console.log("Connected");
	
	//Joining the chat room
	socket.on('set room and name', function(data) {
		socket.join(data.room);
		console.log('joined room: ' + data.room);
		console.log('username: ' + data.name);
		socket.emit('begin chat');
	});
	
	//Sending messages (or receiving)
	socket.on('send msg', function(data) {
		console.log(data.name + ": " + data.text);
		socket.broadcast.to(data.room).emit('msg received', data.name, data.text);
		socket.emit('msg received', data.name, data.text);
	});
	
	socket.on('disconnect', function () {
		console.log("Disconnected");
	});
});
	
server.listen(1337, "127.0.0.1");

